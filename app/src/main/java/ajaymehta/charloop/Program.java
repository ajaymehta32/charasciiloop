package ajaymehta.charloop;

/**
 * Created by Avi Hacker on 7/15/2017.
 */

// here ASCII is in Decimal ...
public class Program {

     // fact of the day  : for loop is just for browsing ...you can browse anything ..even an array with stirng , int , float , object type or anything
    // here we are using to browse though ..char and ascii..

    public static void display() {

        for(char i='A'; i<='Z'; i++) {  // ASCII 65 -- 90

            System.out.print(i+" ");
        }
        System.out.println();
        System.out.println("=======================================");

        for(char i='a'; i<='z'; i++) {  // ASCII 97 - 122

            System.out.print(i+" ");
        }

    }

    public static void main(String args[]) {

        display();

    }
}
