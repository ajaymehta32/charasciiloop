package ajaymehta.charloop;

/**
 * Created by Avi Hacker on 7/15/2017.
 */

public class Program2 {


    public static void display() {

        // you can go from uppercase A to small z  ... coz even if u think in terms of ASCII
        // A -65  , a-97   ...so  small leters are after Capital letters..
        // but you cant go from  small a to uppercase A   its like going from  97 to 65 ..while loop is increating ...(not decreasing)

        for(char i='A'; i<='z'; i++) {    // ASCII 65 - 122   // 91 -96   [ \ ] ^ _ `
            System.out.print(i+" ");
        }

    }

    public static void main(String args[]) {

        display();

    }
}
