package ajaymehta.charloop;

/**
 * Created by Avi Hacker on 7/15/2017.
 */
//char 0 to 20 are unprintiable ...they stand for Value like NULL , STX ,ETX ..etc... you can start printing from .21
public class Program4 {

    public static void display() {

        // when you dont know . character you can do like this  casting n print char

        // here printing char value with with ASCII int value...
        for(char i=(char)65; i<=(char)90; i++) {

            System.out.print(i+"-"+(int)i+", ");  // (int) i ..casting back to int
        }

    }

    public static void main(String args[]) {

        display();

    }
}
