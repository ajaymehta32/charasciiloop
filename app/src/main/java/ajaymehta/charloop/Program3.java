package ajaymehta.charloop;

/**
 * Created by Avi Hacker on 7/15/2017.
 */

public class Program3 {

    public static void display() {

        for(char i=(char)65; i<=(char)90; i++) {  // ASCII 65 -- 90  // casting int into char ..n printing..cool enough..

            System.out.print(i+" ");
        }

    }

    public static void main(String args[]) {

        display();

    }
}
